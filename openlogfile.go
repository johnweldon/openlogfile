package main

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"
)

var (
	now      = time.Now()
	cur      = now.Format("1504")
	today    = now.Format("2006-01-02")
	editor   = os.Getenv("EDITOR")
	root     = os.Getenv("DROPBOX_HOME")
	logfiles = path.Join(root, "logfiles")
	logfile  = path.Join(logfiles, today+".log")
)

func writeFile(path string, lines []string) error {
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return err
	}

	defer f.Close()
	f.WriteString(strings.Join(lines, "\r\n") + "\r\n")
	return nil
}

func verifyFile(path string) error {
	_, err := os.Stat(path)
	if err != nil {
		preamble := []string{
			"Date: " + today,
			"",
		}
		return writeFile(path, preamble)
	}
	return nil
}

func openFileInEditor(path string) error {
	// yes, I realize editor may not be vim like...
	cmd := exec.Command(editor, "--servername", "REPORT", "--remote-silent", path)
	return cmd.Start()
}

func main() {
	flag.Parse()
	args := flag.Args()
	err := verifyFile(logfile)
	if err != nil {
		log.Fatal(err)
	}

	if len(args) > 0 {
		err = writeFile(logfile, []string{cur + " " + strings.Join(args, " ")})
		if err != nil {
			log.Fatal(err)
		}
	} else {
		err = openFileInEditor(logfile)
		if err != nil {
			log.Fatal(err)
		}
	}
}
